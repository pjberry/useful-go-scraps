package main

import (
	"fmt"
	"math/rand"
	"reflect"
	"testing"
	"time"
)

func Test_casts(t *testing.T) {

	type MyStruct struct {
		AString string
	}

	things := []interface{}{1, "2", false, MyStruct{}}

	for _, value := range things {
		typ, ok := value.(string)
		fmt.Println(typ)
		fmt.Println(ok)
		fmt.Println("---")
	}

}

func Test_StructMucking(t *testing.T) {

	s := Outer{
		StringFieldOne: "outer",
		StructField: Inner{
			StringFieldTwo: "inner",
		},
	}

	a := reflect.ValueOf(s)
	b := reflect.ValueOf(&s)
	c := b.Elem()

	fmt.Println(a.CanAddr())
	fmt.Println(b.CanAddr())
	fmt.Println(c.CanAddr())

	fmt.Println("---")
	for i := 0; i < a.NumField(); i++ {
		fld := a.Type().Field(i)
		fmt.Println(fld)

		d := reflect.ValueOf(fld)
		e := reflect.ValueOf(&fld)
		f := e.Elem()
		fmt.Println(d.CanAddr())
		fmt.Println(e.CanAddr())
		fmt.Println(f.CanAddr())
	}

}

func Test_Populate(t *testing.T) {
	s := Outer{}
	ptr := &s
	value := reflect.ValueOf(ptr)
	populateValue(value)

	fmt.Println(s)
	fmt.Println(s.StructField)
	fmt.Println(s.StructField.PointerField.StringFieldThree)
	fmt.Println(s.StructField.PointerField.SliceField)
	fmt.Println(s.AliasedField)
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func populateValue(thing reflect.Value) {
	reflectValue := thing
	reflectType := reflectValue.Type()

	k := reflectType.Kind()
	switch k {
	case reflect.Array:
		fmt.Println("need to implement array")
		fmt.Println(reflectValue.Type())
	case reflect.Bool:
		b := RandomBool()
		randomBool := reflect.ValueOf(b)
		reflectValue.Set(randomBool)
	case reflect.Int:
		i := RandomInt(1, 10)
		randomInt := reflect.ValueOf(i)
		reflectValue.Set(randomInt)
	case reflect.Ptr:
		if reflectValue.IsNil() {
			emptyThing := reflect.New(reflectType.Elem())
			reflectValue.Set(emptyThing)
			populateValue(reflectValue)
		} else {
			populateValue(reflect.Indirect(reflectValue))
		}
	case reflect.Slice:
		containedType := reflectValue.Type().Elem()
		returnSlice := reflect.Indirect(reflect.New(reflectValue.Type()))
		size := RandomInt(1, 10)
		for i := 0; i < size; i++ {
			elementToPopulate := reflect.New(containedType)
			populateValue(elementToPopulate)
			returnSlice = reflect.Append(returnSlice, reflect.Indirect(elementToPopulate))
		}
		reflectValue.Set(returnSlice)
	case reflect.String:
		randomString := RandomString(8)
		reflectValue.Set(reflect.ValueOf(randomString))
	case reflect.Struct:
		for i := 0; i < reflectValue.NumField(); i++ {
			populateValue(reflectValue.Field(i))
		}
	default:
		panic(fmt.Sprintf("Cannot handle kind, %v", k))
	}
}

type Outer struct {
	StringFieldOne string
	StructField    Inner
	PtrString *string
	AliasedField Aliased
}

type Single struct {
	IntField int
}

type Aliased []Single

type Inner struct {
	StringFieldTwo string
	BooleanField   bool
	PointerField   *InnerInner
}

type InnerInner struct {
	StringFieldThree string
	SliceField       []string
}

func RandomString(len int) string {
	bytes := make([]byte, len)
	for i := 0; i < len; i++ {
		bytes[i] = byte(RandomInt(97, 122))
	}
	return string(bytes)
}

func RandomInt(min, max int) int {
	return min + rand.Intn(max-min)
}

func RandomBool() bool {
	return RandomInt(1, 100)%2 == 0
}

func RandomTime() time.Time {
	randomTime := rand.Int63n(time.Now().Unix())
	nanos := rand.Int63n(999999999)
	randomNow := time.Unix(randomTime, nanos)
	return randomNow
}


